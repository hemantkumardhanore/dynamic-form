var dynamicForm = {};
var getJsonData;
dynamicForm.init = function(){
	$('#option_title, #option_body').hide();
	var json_url = 'https://jsonplaceholder.typicode.com/posts';
	getJsonData = jQuery.parseJSON(
        jQuery.ajax({
            url: json_url, 
            async: false,
            dataType: 'json'
        }).responseText
    );	
	jQuery.each(getJsonData, function(i, item) {
		jQuery('#option_id').append('<option>'+item.id+'</option>');
	});
};
loadFormData = function(thisRef){
	var current_id = jQuery(thisRef).val();
	jQuery.each(getJsonData, function(i, item) {
		jQuery('#option_title').append('<option id="title_id'+item.id+'">'+item.title+'</option>');
		if(current_id == item.id){
			if(current_id % 2 !== 0){
				jQuery('#option_title #title_id'+item.id).attr('selected','selected');
				$('#option_title').show();
				$('#option_body').hide();
			}
			else{
				jQuery('#option_body').val(item.body);
				jQuery('#option_title #title_id'+item.id).attr('selected','selected');
				$('#option_title, #option_body').show();
			}
		}
	});
}